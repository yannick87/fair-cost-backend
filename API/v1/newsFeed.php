<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

include_once '../../PHP/autoloader.php';

$db = new Database();

$post = json_decode(file_get_contents("php://input"), true);
$outputArray = [];
$anzahlNews = 3;
if ((isset($post['sessionToken']))) {
    if (SessionToken::checkToken($db, $post['sessionToken'])) {
        $userID = SessionToken::getUserIDByToken($db, $post['sessionToken']);
        $user = new User($db, $userID);
        $wg = new WG($db, $user->getUserWg());

        $outputArray['error'] = false;
        $outputArray['message'] = "successfully";

        $betraege = $wg->getBetraegeWG();

        $outputArray['data'] = [];
        $tempMonth = 0;
        foreach ($betraege as &$b) {
            if(count($outputArray['data']) > 0){
                $actMonth = intval(explode('-',substr($b['betrag_datum'],0,10))[1]);
                if($actMonth != $tempMonth){
                    $a = [];
                    $a['datum'] = explode('-',substr($b['betrag_datum'],0,10))[0] . '-' . explode('-',substr($b['betrag_datum'],0,10))[1] . '-01';
                    $a['news'] = 'Die neue Bilanz wurde erstellt';
                    array_push($outputArray['data'], $a);
                }
                $a = [];
                $a['datum'] = substr($b['betrag_datum'],0,10);
                $a['news'] = $b['user_name'] . ' hat SFr. ' . $b['betrag_wert'] .'.- erfasst';
                array_push($outputArray['data'], $a);
                $tempMonth = $actMonth;
            } else {
                $tempMonth = intval(explode('-',substr($b['betrag_datum'],0,10))[1]);
                $a = [];
                $a['datum'] = substr($b['betrag_datum'],0,10);
                $a['news'] = $b['user_name'] . ' hat SFr. ' . $b['betrag_wert'] .'.- erfasst';
                array_push($outputArray['data'], $a);
            }

            while(count($outputArray['data']) > $anzahlNews) {
                array_shift($outputArray['data']);
            }


        }
    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "token ist not valid";
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));