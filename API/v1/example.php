<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

include_once '../../PHP/autoloader.php';

$db = new Database();
$path = $_SERVER['DOCUMENT_ROOT'].'/backend_fairCost/wg_images/big.jpg';
$imageSize = Helpers::getImageSize($path);
$oldHeight = $imageSize['height'];
$oldWidth = $imageSize['width'];
$newDimensions = Helpers::calcNewImageSize($oldWidth,$oldHeight);
$newHeight = $newDimensions['height'];
$newWidth = $newDimensions['width'];
Helpers::resizeImage($path, $oldWidth, $oldHeight, $newWidth, $newHeight);
