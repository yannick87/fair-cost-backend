<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
$post = json_decode(file_get_contents("php://input"), true);
include_once '../../PHP/autoloader.php';
$db = new Database();

if ((isset($post['sessionToken'])) && (isset($post['betrag']))) {
    if (SessionToken::checkToken($db, $post['sessionToken'])) {
        $comment = "";
        if(isset($post['comment'])){
            $comment = $post['comment'];
        }

        $userID = SessionToken::getUserIDByToken($db, $post['sessionToken']);
        $user = new User($db, $userID);

        $outputArray['error'] = false;
        $outputArray['message'] = "successfully";
        $user->addBetrag($post['betrag'], $comment);

    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "token ist not valid";
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));