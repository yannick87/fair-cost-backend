<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
$post = json_decode(file_get_contents("php://input"), true);
include_once '../../PHP/autoloader.php';
$db = new Database();

if ((isset($post['sessionToken']))) {
    if (SessionToken::checkToken($db, $post['sessionToken'])) {
        $userID = SessionToken::getUserIDByToken($db, $post['sessionToken']);
        $user = new User($db, $userID);
        $wg = new WG($db, $user->getUserWg());
        $users = $wg->getWgUsers();
        $outputArray['error'] = false;
        $outputArray['message'] = "successfully";
        $outputArray['data'] = [];
        foreach($users as &$u){
            $a = [];
            $a['userId'] = $u['user_id'];
            $a['userName'] = $u['user_name'];
            array_push($outputArray['data'], $a);
        }
    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "token ist not valid";
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));