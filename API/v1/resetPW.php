<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

$post = json_decode(file_get_contents("php://input"),true);
include_once '../../PHP/autoloader.php';

$db = new Database();

if ((isset($post['userMail']))) {
    $outputArray = [];
    if(SessionToken::checkEmailExists($db,$post['userMail'])){

        $userId = SessionToken::getUserIDByEmail($db,$post['userMail']);

        $pwr = new PasswortReset($db,"", $userId);


        $receiver = $post['userMail'];
        $subject = 'Password zurücksetzen fairCostApp';
        $message = 'Hallo, Für dein Konto wurde ein neues Passwort angefordert: Bitte klicke auf folgenden Link um dein Passwort zurückzusetzen: https://yweb.ch/backend_fairCost/resetPassword.php?resetToken='.$pwr->getPasswortResetToken();

        Helpers::sendMail($receiver, $subject, $message);

        $outputArray['error'] = 'false';
        $outputArray['message'] = 'Mail with reset link were send to the given email';

    } else {
        $outputArray['error'] = 'true';
        $outputArray['message'] = 'mail unknown';
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));