<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

include_once '../../PHP/autoloader.php';

$db = new Database();

$post = json_decode(file_get_contents("php://input"), true);
$outputArray = [];
if ((isset($post['sessionToken']))) {
    if (SessionToken::checkToken($db, $post['sessionToken'])) {

        $userID = SessionToken::getUserIDByToken($db, $post['sessionToken']);
        $user = new User($db, $userID);
        $wg = new WG($db, $user->getUserWg());

        //prepareWGImage
        $wgImage = $wg->getImage();
        if(($wgImage !== "") && ($wgImage !== null)){
            $imgUrl = $wgImage;
        } else {
            $imgUrl = "https://yweb.ch/backend_fairCost/wg_images/default.jpg";
        }

        $b64image = base64_encode(file_get_contents($imgUrl));

        //prepare Array Output
        $outputArray['error'] = false;
        $outputArray['message'] = "successfully";
        $outputArray['data']['image'] = $b64image;
        $outputArray['data']['wgName'] = $wg->getWgName();

        //check isLeader
        $isLeader = false;
        if($user->getUserIsLeader() == 1){
            $isLeader = true;
        }
        $outputArray['data']['isAdmin'] = $isLeader;
    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "token ist not valid";
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));