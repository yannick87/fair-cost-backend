<?php

function fairCost($betraege)
{
    $sum = 0;
    $people = 0;

    $transactions = [];
    foreach ($betraege as &$b) {
        $sum += $b['betrag'];
        $people++;
        // print_r($b);
    }
    $avg = $sum / $people;
    //$round_avg = round($avg / 0.05) * 0.05;

    $openCosts = [];

    foreach ($betraege as &$b) {
        $a = [];
        $a['name'] = $b['name'];
        $a['betrag'] = $b['betrag'];
        $o = $b['betrag'] - $avg;
        //$o_round = round($o / 0.05) * 0.05;
        $a['open'] =round(($o + 0.000001) * 20) / 20;
        array_push($openCosts, $a);
    }
    //print_r($round_avg);

    $columns = array_column($openCosts, 'open');
    array_multisort($columns, SORT_ASC, $openCosts);


    $i = 0;
    $j = count($openCosts) -1;
    while($i<$j){
        if($openCosts[$i]['open'] != 0 && $openCosts[$j]['open'] != 0){
            if((abs($openCosts[$i]['open']) > abs($openCosts[$j]['open'])) || (($j-$i) == 1)){
                $t['schuldner'] = $openCosts[$i]['name'];
                $t['glaebuiger'] = $openCosts[$j]['name'];
                $t['betrag'] = abs($openCosts[$j]['open']);
                $eq = abs($openCosts[$j]['open']);
                $openCosts[$j]['open'] -= $eq;
                $openCosts[$i]['open'] += $eq;
                $j--;
                if($openCosts[$i]['open'] == 0){
                    $i++;
                }
            } else {
                $t['schuldner'] = $openCosts[$i]['name'];
                $t['glaebuiger'] = $openCosts[$j]['name'];
                $t['betrag'] = abs($openCosts[$i]['open']);
                $eq = abs($openCosts[$i]['open']);
                $openCosts[$i]['open'] += $eq;
                $openCosts[$j]['open'] -= $eq;
                $i++;
                if($openCosts[$i]['open'] == 0){
                    $j--;
                }
            }
            array_push($transactions, $t);
        }
    }
    print_r($transactions);

}

$costs = [['name'=>'Martina', 'betrag'=>0],['name'=>'alain', 'betrag'=>80],['name'=>'yannick', 'betrag'=>90],['name'=>'aline','betrag'=>100],['name'=>'quak','betrag'=>110]];

fairCost($costs);