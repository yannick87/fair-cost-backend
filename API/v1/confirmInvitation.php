<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
$post = json_decode(file_get_contents("php://input"), true);
include_once '../../PHP/autoloader.php';
$db = new Database();

if ((isset($post['userMail'])) && (isset($post['userPassword'])) && (isset($post['userName'])) && (isset($post['wgCode']))) {
    $outputArray = [];
    //check if token is valid
    if (Einladungscode::checkEinladungsCode($db, $post['wgCode'])) {
        //check if email is unique
        $anzahl = 0;
        $stmt = $db->get_dbCon()->prepare("SELECT count(*) as anzahl FROM t_user WHERE user_emailadresse = :user_emailadresse");
        $stmt->bindParam(':user_emailadresse', $post['userMail']);
        $stmt->execute();
        $result = $stmt->fetchAll();
        foreach ($result as &$row) {
            $anzahl = $row['anzahl'];
        }
        if ($anzahl == 0) {
            $e = new Einladungscode($db, 0, $post['wgCode'], "");
            // user is unique create new WG and User
            $wgid = $e->getTWgWgId();
            $wg = new WG($db, $wgid);
            $user = $wg->addUserToWG($post['userName'], $post['userMail'], $post['userPassword'], 0);
            // create Token for new User
            $sessionToken = SessionToken::newToken($db, $user->getUserId());
            $outputArray['error'] = false;
            $outputArray['message'] = "user is created";
            $outputArray['data']['sessionToken'] = $sessionToken;
            Einladungscode::deleteEinladungsCodeFromDB($db, $post['wgCode']);
            unset($e);
        } else {
            $outputArray['error'] = true;
            $outputArray['message'] = "email already exists";
        }
    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "token is not valid";
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}

print_r(json_encode($outputArray));