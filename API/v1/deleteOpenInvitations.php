<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
$post = json_decode(file_get_contents("php://input"), true);
include_once '../../PHP/autoloader.php';
$db = new Database();

if ((isset($post['sessionToken'])) && (isset($post['einladungsCode']))) {
    if (SessionToken::checkToken($db, $post['sessionToken'])) {
        $e = new Einladungscode($db,0,$post['einladungsCode'], "");
        $e->deleteEinladungsCode();
        unset($e);
        $outputArray['error'] = false;
        $outputArray['message'] = "successfully";
    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "token ist not valid";
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));