<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

$post = json_decode(file_get_contents("php://input"), true);
include_once '../../PHP/autoloader.php';

$db = new Database();

if ((isset($post['userMail'])) && (isset($post['userPassword']))) {
    $outputArray = [];
    if (SessionToken::checkCreds($db, $post['userMail'], $post['userPassword'])) {
        $outputArray['error'] = 'false';
        $outputArray['message'] = 'sessionToken setted';
        $userid = SessionToken::getUserID($db, $post['userMail'], $post['userPassword']);
        $user = new User($db, $userid);
        if ($user->getUserStatus() != 10) {
            $outputArray['error'] = 'true';
            $outputArray['message'] = 'user is not active';
        } else {
            $name = $user->getUserName();
            $token = SessionToken::newToken($db, $userid);
            $outputArray['data']['userName'] = $name;
            $outputArray['data']['userId'] = $userid;
            $outputArray['data']['sessionToken'] = $token;
        }
    } else {
        $outputArray['error'] = 'true';
        $outputArray['message'] = 'username or password wrong';
    }

} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));