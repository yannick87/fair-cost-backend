<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

include_once '../../PHP/autoloader.php';

$db = new Database();

$post = json_decode(file_get_contents("php://input"), true);
$outputArray = [];
if ((isset($post['sessionToken']))) {
    if (SessionToken::checkToken($db, $post['sessionToken'])) {
        $userID = SessionToken::getUserIDByToken($db, $post['sessionToken']);
        $user = new User($db, $userID);
        $wg = new WG($db, $user->getUserWg());
        $outputArray['error'] = false;
        $outputArray['message'] = "successfully";
        $outputArray['data'] = [];
        foreach ($wg->getOpenInvitations() as &$r) {
            $a = [];
            $a['provName'] = $r['einladungscode_name'];
            $a['einladungsCode'] = $r['einladungscode_code'];
            array_push($outputArray['data'], $a);
        }
    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "token ist not valid";
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));