<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
$post = json_decode(file_get_contents("php://input"), true);
include_once '../../PHP/autoloader.php';
$db = new Database();

if ((isset($post['sessionToken'])) && (isset($post['month'])) && (isset($post['year']))) {
    if (SessionToken::checkToken($db, $post['sessionToken'])) {

        $userID = SessionToken::getUserIDByToken($db, $post['sessionToken']);
        $user = new User($db, $userID);
        $wg = new WG($db, $user->getUserWg());
        $entries = $wg->getMonthBilanz($post['month'], $post['year']);

        $outputArray['error'] = false;
        $outputArray['message'] = "successfully";
        $outputArray['data'] = [];

        foreach ($entries as &$e) {
            $a = [];
            $a['userName'] = $e['user_name'];
            $a['userId'] = $e['user_id'];
            $a['betragSummeMonat'] = $e['betragSummeMonat'];
            array_push($outputArray['data'], $a);
        }

    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "token ist not valid";
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));