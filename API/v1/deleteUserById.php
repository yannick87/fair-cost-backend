<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
$post = json_decode(file_get_contents("php://input"), true);
include_once '../../PHP/autoloader.php';
$db = new Database();

if ((isset($post['sessionToken'])) && (isset($post['userId']))) {
    if (SessionToken::checkToken($db, $post['sessionToken'])) {
        $user = new User($db, $post['userId']);

        if($user->getUserIsLeader() == 1){
            $wg = new WG($db, $user->getUserWg());
            $users = $wg->getWgUsers();
            $count = count($users);
            if($count > 1){
                $newAdminUser = new User($db, $users[1]['user_id']);
                $newAdminUser->setUserIsLeader(1);
                $user->deleteUser();
                unset($user);
            } else {
                $wg->deleteWG();
            }
        } else {
            $user->deleteUser();
            unset($user);
        }
        $outputArray['error'] = false;
        $outputArray['message'] = "successfully";
    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "token ist not valid";
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));