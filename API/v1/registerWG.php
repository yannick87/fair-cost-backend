<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

include_once '../../PHP/autoloader.php';

$db = new Database();

$post = json_decode(file_get_contents("php://input"), true);
$outputArray = [];
if ((isset($post['userMail'])) && (isset($post['userPassword'])) && (isset($post['userName'])) && (isset($post['wgName']))) {
    //check if email is unique
    $anzahl = 0;
    $stmt = $db->get_dbCon()->prepare("SELECT count(*) AS anzahl FROM t_user WHERE user_emailadresse = :user_emailadresse");
    $stmt->bindParam(':user_emailadresse', $post['userMail']);
    $stmt->execute();
    $result = $stmt->fetchAll();
    foreach ($result as &$row) {
        $anzahl = $row['anzahl'];
    }

    if ($anzahl == 0) {
        // user is unique create new WG and User
        // create WG

        $wgImage = "";
        if ($post['wgImage'] !== "" and $post['wgImage'] !== null) {

            //upload image
            $imgeName = uniqid() . ".jpg";
            $upload_path = "../../wg_images/" . $imgeName;
            file_put_contents($upload_path, base64_decode(utf8_decode(urldecode($post['wgImage']))));
            $wgImage = "https://yweb.ch/backend_fairCost/wg_images/" . $imgeName;

            //resize Image (not heigher than 400px and not widther than 600px)
            $path = $_SERVER['DOCUMENT_ROOT'].'/backend_fairCost/wg_images/'.$imgeName;
            $imageSize = Helpers::getImageSize($path);
            $oldHeight = $imageSize['height'];
            $oldWidth = $imageSize['width'];
            $newDimensions = Helpers::calcNewImageSize($oldWidth,$oldHeight);
            $newHeight = $newDimensions['height'];
            $newWidth = $newDimensions['width'];
            Helpers::resizeImage($path, $oldWidth, $oldHeight, $newWidth, $newHeight);



        }

        $wg = new WG($db, 0, $post['wgName'], $wgImage);
        $user = $wg->addUserToWG($post['userName'], $post['userMail'], $post['userPassword'], 1);


        // create Token for new User
        $sessionToken = SessionToken::newToken($db, $user->getUserId());
        $outputArray['error'] = false;
        $outputArray['message'] = "wg and user were created";
        $outputArray['data']['sessionToken'] = $sessionToken;
    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "email already exists";
    }

} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";

}
print_r(json_encode($outputArray));