<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

include_once '../../PHP/autoloader.php';

$db = new Database();

$post = json_decode(file_get_contents("php://input"), true);
$outputArray = [];
if ((isset($post['sessionToken']))) {
    if (SessionToken::checkToken($db, $post['sessionToken'])) {
        $userID = SessionToken::getUserIDByToken($db, $post['sessionToken']);
        $user = new User($db, $userID);


        $schuldner = $user->getKreditoren();
        $glaeubiger = $user->getDebitoren();

        $openTrans = array_merge($schuldner, $glaeubiger);

        $outputArray['error'] = false;
        $outputArray['message'] = "successfully";
        $outputArray['data'] = [];

        foreach ($openTrans as &$o) {
            $a = [];
            $a['glaeubiger'] = $o['glaeubiger'];
            $a['schuldner'] = $o['schuldner'];
            $a['betrag'] = $o['betrag'];
            $a['schuldenId'] = $o['schuldenId'];
            array_push($outputArray['data'], $a);
        }
    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "token ist not valid";
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));