<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');
$post = json_decode(file_get_contents("php://input"), true);
include_once '../../PHP/autoloader.php';
$db = new Database();

if ((isset($post['sessionToken'])) && (isset($post['betragId']))) {
    if (SessionToken::checkToken($db, $post['sessionToken'])) {
        $betrag = new Betrag($db, $post['betragId']);
        $outputArray['data']['betrag'] = $betrag->getBetragWert();
        $outputArray['data']['comment'] = $betrag->getBetragKommentar();
        $outputArray['data']['datum'] = substr($betrag->getBetragDatum(),0,19);
        $outputArray['error'] = false;
        $outputArray['message'] = "successfully";
    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "token ist not valid";
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));