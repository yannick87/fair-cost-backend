<?php
header('Access-Control-Allow-Origin: *');
header('Content-type: application/json');

include_once '../../PHP/autoloader.php';

$db = new Database();

$post = json_decode(file_get_contents("php://input"), true);
$outputArray = [];
if ((isset($post['sessionToken'])) && (isset($post['provMail'])) && (isset($post['provName']))) {
    if (SessionToken::checkToken($db, $post['sessionToken'])) {


        //check if email is unique
        $anzahl = 0;
        $stmt = $db->get_dbCon()->prepare("SELECT count(*) AS anzahl FROM t_user WHERE user_emailadresse = :user_emailadresse");
        $stmt->bindParam(':user_emailadresse', $post['provMail']);
        $stmt->execute();
        $result = $stmt->fetchAll();
        foreach ($result as &$row) {
            $anzahl = $row['anzahl'];
        }

        if ($anzahl == 0) {
            $userID = SessionToken::getUserIDByToken($db, $post['sessionToken']);
            $user = new User($db, $userID);
            $wg = new WG($db, $user->getUserWg());

            $einladungsCode = new Einladungscode($db, $wg->getWgId(),"", $post['provName']);

            $outputArray['error'] = false;
            $outputArray['message'] = "successfully";

            $receiver = $post['provMail'];
            $subject = 'WG Einladung - FairCostApp';
            $message = 'Hallo ' . $post['provName'] . ', Du hast eine Einladung für eine WG erhalten. Dein Einladungscode lautet: ' . $einladungsCode->getEinladungscodeCode();

            Helpers::sendMail($receiver, $subject, $message);

        } else {
            $outputArray['error'] = true;
            $outputArray['message'] = "email already exists";
        }

    } else {
        $outputArray['error'] = true;
        $outputArray['message'] = "token ist not valid";
    }
} else {
    $outputArray['error'] = true;
    $outputArray['message'] = "Parameter error";
}
print_r(json_encode($outputArray));