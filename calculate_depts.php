<?php
if (isset($_GET['secret']) && $_GET['secret'] == '777Nase') {
    $host = 'localhost';
    $db = 'fairCostDB';
    $user = 'fairCostUser';
    $pw = '7777Nase$';

    try {
        $dbCon = new PDO("mysql:host=" . $host . ";dbname=" . $db, $user, $pw);
        $dbCon->exec("set names utf8");
    } catch (PDOException $exception) {
        echo "Connection error: " . $exception->getMessage();
    }

//schuldenmonat
    $year = date('Y');
    $month = date('m');
    $vormonat = -1;
    $vormonatString = "";

    if ($month == 1) {
        $vormonat = 12;
        $vormonatString = "" . $vormonat;
        $year = $year - 1;
    } else {
        $vormonat = $month - 1;
        if ($vormonat < 10) {
            $vormonatString = "0" . $vormonat;
        } else {
            $vormonatString = "" . $vormonat;
        }
    }

    $schuldenmonat = $vormonatString . $year;

    function fairCost($betraege, $dbCon, $schuldenmonat)
    {
        $sum = 0;
        $people = 0;

        $transactions = [];
        foreach ($betraege as &$b) {
            $sum += $b['betrag'];
            $people++;
            // print_r($b);
        }
        $avg = $sum / $people;
        //$round_avg = round($avg / 0.05) * 0.05;

        $openCosts = [];

        foreach ($betraege as &$b) {
            $a = [];
            $a['name'] = $b['name'];
            $a['betrag'] = $b['betrag'];
            $o = $b['betrag'] - $avg;
            //$o_round = round($o / 0.05) * 0.05;
            $a['open'] = round(($o + 0.000001) * 20) / 20;
            array_push($openCosts, $a);
        }
        //print_r($round_avg);

        $columns = array_column($openCosts, 'open');
        array_multisort($columns, SORT_ASC, $openCosts);


        $i = 0;
        $j = count($openCosts) - 1;
        while ($i < $j) {
            if ($openCosts[$i]['open'] != 0 && $openCosts[$j]['open'] != 0) {
                if ((abs($openCosts[$i]['open']) > abs($openCosts[$j]['open'])) || (($j - $i) == 1)) {
                    $t['schuldner'] = $openCosts[$i]['name'];
                    $t['glaebuiger'] = $openCosts[$j]['name'];
                    $t['betrag'] = abs($openCosts[$j]['open']);
                    $eq = abs($openCosts[$j]['open']);
                    $openCosts[$j]['open'] -= $eq;
                    $openCosts[$i]['open'] += $eq;
                    $j--;
                    if ($openCosts[$i]['open'] == 0) {
                        $i++;
                    }
                } else {
                    $t['schuldner'] = $openCosts[$i]['name'];
                    $t['glaeubiger'] = $openCosts[$j]['name'];
                    $t['betrag'] = abs($openCosts[$i]['open']);
                    $eq = abs($openCosts[$i]['open']);
                    $openCosts[$i]['open'] += $eq;
                    $openCosts[$j]['open'] -= $eq;
                    $i++;
                    if ($openCosts[$i]['open'] == 0) {
                        $j--;
                    }
                }
                array_push($transactions, $t);
            }
        }

        foreach ($transactions as &$t) {
            writeDeptsToDB($dbCon, $t['schuldner'], $t['glaeubiger'], $t['betrag'], $schuldenmonat);
        }
    }

    function writeDeptsToDB($dbCon, $schuldner, $glaeubiger, $betrag, $schuldenmonat)
    {
        $stmt = $dbCon->prepare("INSERT INTO t_schulden (schulden_monat_jahr, schulden_schuldenbetrag, schulden_beglichen, schulden_glaubiger_user_id, schulden_schuldner_user_id1) VALUES('" . $schuldenmonat . "', :schulden_schuldenbetrag, 0, :schulden_glaubiger_user_id, :schulden_schuldner_user_id1)");
        $stmt->bindParam(':schulden_schuldenbetrag', $betrag);
        $stmt->bindParam(':schulden_glaubiger_user_id', $glaeubiger);
        $stmt->bindParam(':schulden_schuldner_user_id1', $schuldner);
        $stmt->execute();
    }


    $wgs = [];

    $stmt = $dbCon->prepare("SELECT wg_id FROM t_wg");
// $stmt->bindParam(':wg_name', $wgName);
    $stmt->execute();

    $result = $stmt->fetchAll();
    foreach ($result as &$row) {

        array_push($wgs, $row['wg_id']);
    }

//get Users for each WG
    $users = [];
    foreach ($wgs as &$wg) {
        $users = [];
        $stmt = $dbCon->prepare("SELECT SUM(betrag_wert) betragsumme, betrag_user_id FROM t_betrag as t
JOIN t_user as u on t.betrag_user_id = u.user_id
WHERE 1=1
AND u.user_wg_id = " . $wg . "
AND betrag_datum >= (LAST_DAY(CURDATE() - INTERVAL 2 MONTH) + INTERVAL 1 DAY)
AND betrag_datum < (LAST_DAY(CURDATE() - INTERVAL 1 MONTH) + INTERVAL 1 DAY)
GROUP By betrag_user_id");
// $stmt->bindParam(':wg_name', $wgName);
        $stmt->execute();
        $result = $stmt->fetchAll();

        foreach ($result as &$row) {
            $u = [];
            $u['name'] = $row['betrag_user_id'];
            $u['betrag'] = $row['betragsumme'];
            array_push($users, $u);

        }

        if (count($users) > 0) {
            fairCost($users, $dbCon, $schuldenmonat);
        }

    }
}
