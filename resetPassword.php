<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Passwort zurücksetzen - FairCostApp</title>

    <!-- Bootstrap-CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- Besondere Stile für diese Vorlage -->
    <link href="css/signin.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
<div class="container">

    <?php
    include_once 'PHP/autoloader.php';
    $db = new Database();
    if (isset($_GET['resetToken']) && $_GET['resetToken'] != "") {
        if (isset($_POST['submitButton'])) {
            if (($_POST['pw1'] === $_POST['pw2']) && ($_POST['pw1'] !== "") && ($_POST['pw2'] !== "")) {
                if (SessionToken::checkResetTokenExists($db, $_GET['resetToken'])) {
                    $pr = new PasswortReset($db, $_GET['resetToken']);
                    $user = new User($db, $pr->getPasswortResetUserId());
                    $user->setUserPasswort(md5($_POST['pw1']));
                    $pr->deletePasswortReset();
                    unset($pr);
                    ?>
                    <div class="panel panel-success">
                        <div class="panel-heading">
                            <h3 class="panel-title">Speichern erfolgreich</h3>
                        </div>
                        <div class="panel-body">
                            Das neue Passwort wurde gesetzt
                        </div>
                    </div>
                    <?php
                } else {
                    ?>
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">Fehler</h3>
                        </div>
                        <div class="panel-body">
                            Ungültiger Link!
                        </div>
                    </div>
                    <?php
                }
            } else {
                if (SessionToken::checkResetTokenExists($db, $_GET['resetToken'])) {
                    $pr = new PasswortReset($db, $_GET['resetToken']);
                    if ($pr->isPasswordNotExpired()) {
                        ?>
                        <form method="post"
                              action="<?php echo($_SERVER['PHP_SELF'] . "?resetToken=" . $_GET['resetToken']); ?>">
                            <h2 class="form-signin-heading">Neues Passwort</h2>
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Fehler</h3>
                                </div>
                                <div class="panel-body">
                                    Passwörter stimmen nich überein!
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="pw1">Gib das neue Passwort ein</label>
                                <input type="password" class="form-control pwFields" id="pw1" name="pw1"
                                       placeholder="Passwort">
                            </div>
                            <div class="form-group">
                                <label for="pw2">Passwort wiederholen</label>
                                <input type="password" class="form-control pwFields" id="pw2" name="pw2"
                                       placeholder="Passwort wiederholen">
                            </div>
                            <button id="submitButton" name="submitButton" class="btn btn-default">Speichern</button>
                        </form>
                        <?php

                    } else {
                        ?>
                        <div class="panel panel-danger">
                            <div class="panel-heading">
                                <h3 class="panel-title">Fehler</h3>
                            </div>
                            <div class="panel-body">
                                Link ist abgelaufen!
                            </div>
                        </div>
                        <?php
                    }
                } else {
                    ?>
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">Fehler</h3>
                        </div>
                        <div class="panel-body">
                            Ungültiger Link!
                        </div>
                    </div>
                    <?php
                }
            }
        } else {
            if (SessionToken::checkResetTokenExists($db, $_GET['resetToken'])) {
                $pr = new PasswortReset($db, $_GET['resetToken']);
                if ($pr->isPasswordNotExpired()) {
                    ?>
                    <form method="post"
                          action="<?php echo($_SERVER['PHP_SELF'] . "?resetToken=" . $_GET['resetToken']); ?>">
                        <h2 class="form-signin-heading">Neues Passwort</h2>
                        <div class="form-group">
                            <label for="pw1">Gib das neue Passwort ein</label>
                            <input type="password" class="form-control pwFields" id="pw1" name="pw1"
                                   placeholder="Passwort">
                        </div>
                        <div class="form-group">
                            <label for="pw2">Passwort wiederholen</label>
                            <input type="password" class="form-control pwFields" id="pw2" name="pw2"
                                   placeholder="Passwort wiederholen">
                        </div>
                        <button id="submitButton" name="submitButton" class="btn btn-default">Speichern</button>
                    </form>
                    <?php

                } else {
                    ?>
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">Fehler</h3>
                        </div>
                        <div class="panel-body">
                            Link ist abgelaufen!
                        </div>
                    </div>
                    <?php
                }
            } else {
                ?>
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">Fehler</h3>
                    </div>
                    <div class="panel-body">
                        Ungültiger Link!
                    </div>
                </div>
                <?php
            }
        }
    } else {
        ?>
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Fehler</h3>
            </div>
            <div class="panel-body">
                Ungültiger Link!
            </div>
        </div>
        <?php
    }
    ?>

</div> <!-- /container -->
</body>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/script.js"></script>
</html>

