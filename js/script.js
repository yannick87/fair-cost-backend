$(document).ready(function() {
    $('#submitButton').prop('disabled', true);

    $(document).on("input", ".pwFields", function () {
        if(($('#pw1').val() != "") && ($('#pw2').val() != "")){
            $('#submitButton').prop('disabled', false);
        } else {
            $('#submitButton').prop('disabled', true);
        }
    });
});