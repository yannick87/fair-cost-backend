<?php

class WG
{
    private $db;

    private $wg_id;
    private $wg_name;
    private $wg_image;

    private function writeDataToDB(){
        $stmt = $this->db->get_dbCon()->prepare("UPDATE t_wg SET wg_name = :wg_name, wg_image = :wg_image WHERE wg_id = :wg_id");
        $stmt->bindParam(':wg_name', $this->wg_name);
        $stmt->bindParam(':wg_image', $this->wg_image);
        $stmt->bindParam(':wg_id', $this->wg_id);
        $stmt->execute();
        return ($this->db->get_dbCon()->lastInsertId());
    }

    private function createNewWG($wg_name, $wg_image)
    {
        $stmt = $this->db->get_dbCon()->prepare("INSERT INTO t_wg (wg_name, wg_image) VALUES(:wg_name, :wg_image)");
        $stmt->bindParam(':wg_name', $wg_name);
        $stmt->bindParam(':wg_image', $wg_image);
        $stmt->execute();
        return ($this->db->get_dbCon()->lastInsertId());
    }

    private function readWGFromDB()
    {
        $stmt = $this->db->get_dbCon()->prepare("SELECT * FROM t_wg WHERE wg_id = :wg_id");
        $stmt->bindParam(':wg_id', $this->wg_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        foreach ($result as &$row) {
            $this->wg_name = $row['wg_name'];
            $this->wg_image = $row['wg_image'];
        }
    }

    public function __construct($db, $wg_id = 0, $wg_name = "", $wg_image = "")
    {
        $this->db = $db;

        if($wg_image !== "" and $wg_image !== null){
            $this->wg_image = $wg_image;
        } else {
            $this->wg_image = "";
        }

        if (($wg_id <= 0) && ($wg_name != "")) {
            //create New WG
            $this->wg_id = $this->createNewWG($wg_name, $this->wg_image);
        } else {
            $this->wg_id = $wg_id;
        }

        $this->readWGFromDB();
    }

    public function getWgId()
    {
        return $this->wg_id;
    }

    public function getWgName()
    {
        return $this->wg_name;
    }

    public function setWgName($wg_name)
    {
        $this->wg_name = $wg_name;
        $this->writeDataToDB();
    }

    public function addUserToWG($user_name, $user_emailadresse, $user_passwort, $user_isLeader)
    {
        return (new User($this->db, 0, $user_name, $user_emailadresse, $this->wg_id, $user_passwort, $user_isLeader));
    }

    public function getOpenInvitations()
    {
        $stmt = $this->db->get_dbCon()->prepare("SELECT * FROM t_einladungscode WHERE t_wg_wg_id = :wg_id");
        $stmt->bindParam(':wg_id', $this->wg_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getWgUsers()
    {
        $stmt = $this->db->get_dbCon()->prepare("SELECT * FROM t_user WHERE user_wg_id = :user_wg_id and user_status_id = 10");
        $stmt->bindParam(':user_wg_id', $this->wg_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getMonthBilanz($month, $year)
    {
        $stmt = $this->db->get_dbCon()->prepare("SELECT user_id, user_name, round(sum(betrag_wert),2) as betragSummeMonat FROM t_user JOIN t_betrag ON betrag_user_id = user_id WHERE user_wg_id = :user_wg_id and betrag_datum >= :betrag_datumStart and betrag_datum < :betrag_datumEnd GROUP BY user_id, user_name");
        $stmt->bindParam(':user_wg_id', $this->wg_id);
        $stmt->bindParam(':betrag_datumStart', Helpers::createStartDateString($month, $year));
        $stmt->bindParam(':betrag_datumEnd', Helpers::createEndDateString($month, $year));
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function deleteWG() {
        $stmt = $this->db->get_dbCon()->prepare("DELETE FROM t_wg WHERE wg_id = :wg_id");
        $stmt->bindParam(':wg_id', $this->wg_id);
        $stmt->execute();
    }

    public function getBetraegeWG(){
        $stmt = $this->db->get_dbCon()->prepare("SELECT b.betrag_datum, u.user_name, b.betrag_wert FROM t_betrag AS b
JOIN t_user AS u ON b.betrag_user_id = u.user_id
JOIN t_wg AS w ON w.wg_id = u.user_wg_id
WHERE wg_id = :wg_id
ORDER BY b.betrag_datum ASC");
        $stmt->bindParam(':wg_id', $this->wg_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function setWGImage($imagePath){
        $this->wg_image = $imagePath;
        $this->writeDataToDB();
    }

    public function getImage(){
        return $this->wg_image;
    }
}