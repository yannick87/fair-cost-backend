<?php
require_once 'Betrag.php';

class User
{
    private $db;

    private $user_id;
    private $user_name;
    private $user_emailadresse;
    private $user_passwort;
    private $user_isLeader;
    private $user_status;
    private $user_wg;

    private function readUserFromDB()
    {
        $stmt = $this->db->get_dbCon()->prepare("SELECT * FROM t_user WHERE user_id = :user_id");
        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        foreach ($result as &$row) {
            $this->user_id = $row['user_id'];
            $this->user_name = $row['user_name'];
            $this->user_emailadresse = $row['user_emailadresse'];
            $this->user_passwort = $row['user_passwort'];
            $this->user_isLeader = $row['user_isleader'];
            $this->user_wg = $row['user_wg_id'];
            $this->user_status = $row['user_status_id'];
        }
    }

    private function createNewUser($user_emailadresse, $user_name, $user_wg_id, $user_passwort, $user_isLeader)
    {
        $stmt = $this->db->get_dbCon()->prepare("INSERT INTO t_user (user_name, user_emailadresse, user_passwort, user_wg_id, user_isLeader, user_status_id) VALUES(:user_name, :user_emailadresse, :user_passwort, :user_wg_id, :user_isLeader, 10)");
        $stmt->bindParam(':user_name', $user_name);
        $stmt->bindParam(':user_emailadresse', $user_emailadresse);
        $stmt->bindParam(':user_passwort', $user_passwort);
        $stmt->bindParam(':user_wg_id', $user_wg_id);
        $stmt->bindParam(':user_isLeader', $user_isLeader);
        $stmt->execute();
        return ($this->db->get_dbCon()->lastInsertId());
    }

    private function writeUserToDB(){
        $stmt = $this->db->get_dbCon()->prepare("UPDATE t_user SET user_name = :user_name, user_emailadresse = :user_emailadresse, user_passwort = :user_passwort, user_wg_id = :user_wg_id, user_isLeader = :user_isLeader, user_status_id = :user_status_id WHERE user_id = :user_id");
        $stmt->bindParam(':user_name', $this->user_name);
        $stmt->bindParam(':user_emailadresse', $this->user_emailadresse);
        $stmt->bindParam(':user_passwort', $this->user_passwort);
        $stmt->bindParam(':user_wg_id', $this->user_wg);
        $stmt->bindParam(':user_isLeader', $this->user_isLeader);
        $stmt->bindParam(':user_status_id', $this->user_status);
        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->execute();
    }

    public function __construct($db, $user_id = 0, $user_name = "", $user_emailadresse = "", $user_wg_id = 0, $user_passwort = "", $user_isLeader = 0)
    {
        $this->db = $db;
        if (($user_id <= 0) && ($user_name != "") && ($user_emailadresse != "") && ($user_wg_id > 0) && ($user_passwort != "")) {
            //create New User
            $this->user_id = $this->createNewUser($user_emailadresse, $user_name, $user_wg_id, $user_passwort, $user_isLeader);
        } else {
            $this->user_id = $user_id;
        }
        $this->readUserFromDB();
    }

    public function setUserPasswort($user_passwort)
    {
        $this->user_passwort = $user_passwort;
        $this->writeUserToDB();
    }

    public function getUserId()
    {
        return $this->user_id;
    }

    public function getUserName()
    {
        return $this->user_name;
    }

    public function getUserEmailadresse()
    {
        return $this->user_emailadresse;
    }

    public function setUserEmailadresse($user_emailadresse)
    {
        $this->user_emailadresse = $user_emailadresse;
        $this->writeUserToDB();
    }

    public function getUserIsLeader()
    {
        return $this->user_isLeader;
    }

    public function setUserIsLeader($user_isLeader)
    {
        $this->user_isLeader = $user_isLeader;
        $this->writeUserToDB();
    }

    public function getUserStatus()
    {
        return $this->user_status;
    }

    public function setUserStatus($user_status)
    {
        $this->user_status = $user_status;
        $this->writeUserToDB();
    }

    public function getUserWg()
    {
        return $this->user_wg;
    }

    public function addBetrag($betrag_betrag, $betrag_beschreibung)
    {
        $b = new Betrag($this->db, 0, $this->user_id, $betrag_betrag, $betrag_beschreibung);
        return $b;
    }

    public function getBetraegeByMonth($month, $year)
    {
        $stmt = $this->db->get_dbCon()->prepare("SELECT betrag_id, betrag_datum, round(betrag_wert,2) as betrag_wert FROM t_betrag WHERE betrag_user_id = :betrag_user_id and betrag_datum >= :betrag_datumStart and betrag_datum < :betrag_datumEnd");
        $stmt->bindParam(':betrag_user_id', $this->user_id);
        $stmt->bindParam(':betrag_datumStart', Helpers::createStartDateString($month, $year));
        $stmt->bindParam(':betrag_datumEnd', Helpers::createEndDateString($month, $year));
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function deleteUser(){
        $this->setUserStatus(11);
        $this->setUserIsLeader(0);
        $this->setUserEmailadresse('user-' . $this->user_id . '-deleted');
        SessionToken::unsetToken($this->db, $this->user_id);
    }

    public function getKreditoren(){
        $stmt = $this->db->get_dbCon()->prepare("SELECT schulden_id as schuldenId, u1.user_name as glaeubiger, u2.user_name as schuldner, schulden_schuldenbetrag as betrag FROM t_schulden 
JOIN t_user as u1 on u1.user_id = schulden_glaubiger_user_id
JOIN t_user as u2 on u2.user_id = schulden_schuldner_user_id1
WHERE schulden_schuldner_user_id1 = :schulden_schuldner_user_id1 AND schulden_beglichen = 0");
        $stmt->bindParam(':schulden_schuldner_user_id1', $this->user_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getDebitoren(){
        $stmt = $this->db->get_dbCon()->prepare("SELECT schulden_id as schuldenId, u1.user_name as glaeubiger, u2.user_name as schuldner, schulden_schuldenbetrag as betrag FROM t_schulden 
JOIN t_user as u1 on u1.user_id = schulden_glaubiger_user_id
JOIN t_user as u2 on u2.user_id = schulden_schuldner_user_id1
WHERE schulden_glaubiger_user_id = :schulden_glaubiger_user_id AND schulden_beglichen = 0");
        $stmt->bindParam(':schulden_glaubiger_user_id', $this->user_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

}