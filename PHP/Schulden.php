<?php

class Schulden
{
    private $db;
    private $schulden_id;
    private $schulden_monat_jahr;
    private $schulden_schuldenbetrag;
    private $schulden_beglichen;
    private $schulden_glaubiger_user_id;
    private $schulden_schuldner_user_id;

    public function __construct($db, $schulden_id = 0, $schulden_monat_jahr = "", $schulden_schuldenbetrag = 0, $schulden_glaubiger_user_id = 0, $schulden_schuldner_user_id = 0)
    {
        $this->db = $db;
        if (($schulden_id == 0) && ($schulden_monat_jahr == "") && ($schulden_schuldenbetrag != 0) && ($schulden_glaubiger_user_id != 0) && ($schulden_schuldner_user_id != 0)) {
            //create New Schulden
            $this->schulden_id = $this->createNewSchulden($schulden_monat_jahr, $schulden_schuldenbetrag, $schulden_glaubiger_user_id, $schulden_schuldner_user_id);
        } else {
            $this->schulden_id = $schulden_id;
        }
        $this->readSchuldenFromDB();
    }

    private function createNewSchulden($schulden_monat_jahr, $schulden_schuldenbetrag, $schulden_glaubiger_user_id, $schulden_schuldner_user_id)
    {
        $stmt = $this->db->get_dbCon()->prepare("INSERT INTO t_schulden (schulden_monat_jahr, schulden_schuldenbetrag, schulden_beglichen, schulden_glaubiger_user_id, schulden_schuldner_user_id1) VALUES(:schulden_monat_jahr, :schulden_schuldenbetrag, 0, :schulden_glaubiger_user_id, :schulden_schuldner_user_id1)");
        $stmt->bindParam(':schulden_monat_jahr', $schulden_monat_jahr);
        $stmt->bindParam(':schulden_schuldenbetrag', $schulden_schuldenbetrag);
        $stmt->bindParam(':schulden_glaubiger_user_id', $schulden_glaubiger_user_id);
        $stmt->bindParam(':schulden_schuldner_user_id1', $schulden_schuldner_user_id);
        $stmt->execute();
        return ($this->db->get_dbCon()->lastInsertId());
    }

    private function readSchuldenFromDB()
    {
        $stmt = $this->db->get_dbCon()->prepare("SELECT * FROM t_schulden WHERE schulden_id = :schulden_id");
        $stmt->bindParam(':schulden_id', $this->schulden_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        foreach ($result as &$row) {
            $this->schulden_monat_jahr = $row['schulden_monat_jahr'];
            $this->schulden_schuldenbetrag = $row['schulden_schuldenbetrag'];
            $this->schulden_beglichen = $row['schulden_beglichen'];
            $this->schulden_glaubiger_user_id = $row['schulden_glaubiger_user_id'];
            $this->schulden_schuldner_user_id = $row['schulden_schuldner_user_id1'];
        }
    }

    private function writeSchuldenToDB(){
        $stmt = $this->db->get_dbCon()->prepare("UPDATE t_schulden SET schulden_monat_jahr = :schulden_monat_jahr, schulden_schuldenbetrag = :schulden_schuldenbetrag, schulden_beglichen = :schulden_beglichen, schulden_beglichen = :schulden_beglichen, schulden_glaubiger_user_id = :schulden_glaubiger_user_id, schulden_schuldner_user_id1 = :schulden_schuldner_user_id WHERE schulden_id = :schulden_id");
        $stmt->bindParam(':schulden_monat_jahr', $this->schulden_monat_jahr);
        $stmt->bindParam(':schulden_schuldenbetrag', $this->schulden_schuldenbetrag);
        $stmt->bindParam(':schulden_beglichen', $this->schulden_beglichen);
        $stmt->bindParam(':schulden_glaubiger_user_id', $this->schulden_glaubiger_user_id);
        $stmt->bindParam(':schulden_schuldner_user_id', $this->schulden_schuldner_user_id);
        $stmt->bindParam(':schulden_id', $this->schulden_id);
        $stmt->execute();
    }

    public function getSchuldenBeglichen()
    {
        return $this->schulden_beglichen;
    }

    public function setSchuldenBeglichen($schulden_beglichen)
    {
        $this->schulden_beglichen = $schulden_beglichen;
        $this->writeSchuldenToDB();
    }

    public function getSchuldenMonatJahr()
    {
        return $this->schulden_monat_jahr;
    }

    public function getSchuldenSchuldenbetrag()
    {
        return $this->schulden_schuldenbetrag;
    }

    public function getSchuldenGlaubigerUserId()
    {
        return $this->schulden_glaubiger_user_id;
    }

    public function getSchuldenSchuldnerUserId()
    {
        return $this->schulden_schuldner_user_id;
    }

}