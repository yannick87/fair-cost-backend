<?php
class PasswortReset
{
    private $db;

    private $passwortReset_Token;
    private $passwortReset_user_id;
    private $passwortReset_expired;

    private function checkRequestIsUnique($user_id){
        $stmt = $this->db->get_dbCon()->prepare("SELECT * FROM t_passwortReset WHERE passwortReset_user_id = :passwortReset_user_id");
        $stmt->bindParam(':passwortReset_user_id', $user_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $rows = count($result);
        if($rows >= 1){
            return false;
        } else {
            return true;
        }
    }

    private function createNewPasswortReset($user_id)
    {
        $resetToken = "";
        for ($i = 1; $i <= 4; $i++) {
            $resetToken .= uniqid() . '-';
        }
        $resetToken = substr($resetToken, 0, -1);

        if($this->checkRequestIsUnique($user_id)){
            $stmt = $this->db->get_dbCon()->prepare("INSERT INTO t_passwortReset (passwortReset_Token, passwortReset_user_id, passwortReset_expired) VALUES(:passwortReset_Token, :passwortReset_user_id, ADDTIME(now(), '02:00:00'))");
            $stmt->bindParam(':passwortReset_Token', $resetToken);
            $stmt->bindParam(':passwortReset_user_id', $user_id);
            $stmt->execute();
        } else {
            $stmt = $this->db->get_dbCon()->prepare("UPDATE t_passwortReset SET passwortReset_Token = :passwortReset_Token, passwortReset_expired = ADDTIME(now(), '02:00:00') WHERE passwortReset_user_id = :passwortReset_user_id");
            $stmt->bindParam(':passwortReset_Token', $resetToken);
            $stmt->bindParam(':passwortReset_user_id', $user_id);
            $stmt->execute();
        }
        return $resetToken;

    }

    private function readPasswortReset()
    {
        $stmt = $this->db->get_dbCon()->prepare("SELECT * FROM t_passwortReset WHERE passwortReset_Token = :passwortReset_Token");
        $stmt->bindParam(':passwortReset_Token', $this->passwortReset_Token);
        $stmt->execute();
        $result = $stmt->fetchAll();
        foreach ($result as &$row) {
            $this->passwortReset_user_id = $row['passwortReset_user_id'];
            $this->passwortReset_expired = $row['passwortReset_expired'];
        }
    }

    public function __construct($db, $passwortReset_Token = "", $passwortReset_user_id = 0)
    {
        $this->db = $db;
        if (($passwortReset_user_id != 0) && ($passwortReset_Token == "")) {
            //create New PW Reset
            $this->passwortReset_Token = $this->createNewPasswortReset($passwortReset_user_id);
        } else {
            $this->passwortReset_Token = $passwortReset_Token;
        }
        $this->readPasswortReset();
    }

    public function deletePasswortReset(){
        $stmt = $this->db->get_dbCon()->prepare("DELETE FROM t_passwortReset WHERE passwortReset_Token = :passwortReset_Token");
        $stmt->bindParam(':passwortReset_Token', $this->passwortReset_Token);
        $stmt->execute();
    }

    public function getPasswortResetToken()
    {
        return $this->passwortReset_Token;
    }

    public function isPasswordNotExpired(){
        $phpTime = date("Y-m-d H:i:s");
        $expire = new DateTime($this->passwortReset_expired);
        $expire = $expire->format("Y-m-d H:i:s");

        if($expire < $phpTime){
            return false;
        } else {
            return true;
        }
    }

    public function getPasswortResetUserId()
    {
        return $this->passwortReset_user_id;
    }

}