<?php
class Betrag
{
    private $db;
    private $betrag_id;
    private $betrag_user_id;
    private $betrag_wert;
    private $betrag_kommentar;
    private $betrag_datum;

    private function createNewBetrag($betrag_user_id, $betrag_wert, $betrag_kommentar)
    {
        $stmt = $this->db->get_dbCon()->prepare("INSERT INTO t_betrag (betrag_user_id, betrag_wert, betrag_kommentar) VALUES(:betrag_user_id,:betrag_wert,:betrag_kommentar)");
        $stmt->bindParam(':betrag_user_id', $betrag_user_id);
        $stmt->bindParam(':betrag_wert', $betrag_wert);
        $stmt->bindParam(':betrag_kommentar', $betrag_kommentar);
        $stmt->execute();
    }

    private function readBetragFromDB(){
        $stmt = $this->db->get_dbCon()->prepare("SELECT * FROM t_betrag WHERE Betrag_Id = :Betrag_Id");
        $stmt->bindParam(':Betrag_Id', $this->betrag_id);
        $stmt->execute();
        $result = $stmt->fetchAll();
        foreach ($result as &$row) {
            $this->betrag_user_id = $row['betrag_user_id'];
            $this->betrag_wert = $row['betrag_wert'];
            $this->betrag_kommentar = $row['betrag_kommentar'];
            $this->betrag_datum = $row['betrag_datum'];
        }
    }

    public function __construct($db, $betrag_id = 0, $betrag_user_id = 0, $betrag_wert = 0, $betrag_kommentar = "")
    {
        $this->db = $db;
        if (($betrag_id <= 0) && ($betrag_user_id > 0) && ($betrag_wert > 0)) {
            //create New Betrag
            $this->betrag_id = $this->createNewBetrag($betrag_user_id, $betrag_wert, $betrag_kommentar);
        } else {
            $this->betrag_id = $betrag_id;
        }
        $this->readBetragFromDB();
    }

    public function getBetragWert()
    {
        return $this->betrag_wert;
    }

    public function getBetragKommentar()
    {
        return $this->betrag_kommentar;
    }

    public function getBetragDatum()
    {
        return $this->betrag_datum;
    }

    public function deleteBetrag(){
        $stmt = $this->db->get_dbCon()->prepare("DELETE FROM t_betrag WHERE betrag_id = :betrag_id");
        $stmt->bindParam(':betrag_id', $this->betrag_id);
        $stmt->execute();
    }
}