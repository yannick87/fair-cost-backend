<?php

class Helpers
{

    public static function monthToStringWithZero($month)
    {
        if ($month <= 9) {
            return '0' . $month;
        } else {
            return '' . $month;
        }
    }

    public static function createStartDateString($month, $year)
    {
        return $year . '-' . Helpers::monthToStringWithZero($month) . '-01';
    }

    public static function createEndDateString($month, $year)
    {
        if ($month < 12) {
            $month = $month + 1;
            return $year . '-' . Helpers::monthToStringWithZero($month) . '-01';
        } else {
            $year = $year + 1;
            $month = 1;
            return $year . '-' . Helpers::monthToStringWithZero($month) . '-01';
        }
    }

    public static function sendMail($r, $s, $m){

        $sender = 'hello@fairCostApp.ch';
        $receiver = $r;
        $subject = $s;
        $message = $m;

        $headersSystemMail[] = "MIME-Version: 1.0";
        $headersSystemMail[] = "Content-type: text/plain; charset=utf-8";
        $headersSystemMail[] = "From: " . $sender;
        $headersSystemMail[] = "Reply-To: " . $sender;
        $headersSystemMail[] = "X-Mailer: PHP/" . phpversion();

        mail($receiver, $subject, $message, implode("\r\n", $headersSystemMail));
    }

    public static function getImageSize($imagePath){
        list($width, $height) = getimagesize($imagePath);
        $dimension['width'] = $width;
        $dimension['height'] = $height;
        return $dimension;
    }

    public static function calcNewImageSize($width, $height){
        if($width > $height){
            //Image ist breiter als hoch
            $newWidth = 600;
            $newHeight = $height / $width * $newWidth;
        } else {
            //Image ist höher als breit
            $newHeight = 300;
            $newWidth = $width / $height * $newHeight;
        }
        $newDimensions['width'] = round($newWidth);
        $newDimensions['height'] = round($newHeight);
        return $newDimensions;
    }

    public static function resizeImage($imagePath, $oldWidth, $oldHeight, $newWidth, $newHeight){
        $image_p = imagecreatetruecolor($newWidth, $newHeight);
        $image = imagecreatefromjpeg($imagePath);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);
        imagejpeg($image_p, $imagePath, 100);
    }
}