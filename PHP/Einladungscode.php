<?php
class Einladungscode
{
    private $db;

    private $einladungscode_code;
    private $t_wg_wg_id;
    private $einladungscode_name;

    public function __construct($db, $t_wg_wg_id = 0, $einladungscode_code = "", $einladungscode_name = "")
    {
        $this->db = $db;
        if (($t_wg_wg_id != 0) && ($einladungscode_code == "") && ($einladungscode_name != "")) {
            //create New Einladungscode
            $this->einladungscode_code = $this->createNewEinladungscode($t_wg_wg_id, $einladungscode_name);
        } else {
            $this->einladungscode_code = $einladungscode_code;
        }
        $this->readEinladungFromDB($this->einladungscode_code);
    }

    public function getEinladungscodeCode()
    {
        return $this->einladungscode_code;
    }

    public function getTWgWgId()
    {
        return $this->t_wg_wg_id;
    }

    public function getEinladungscodeName()
    {
        return $this->einladungscode_name;
    }

    private function createNewEinladungscode($t_wg_wg_id, $einladungscode_name)
    {
        $einladungsToken = "";
        for ($i = 1; $i <= 4; $i++) {
            $einladungsToken .= uniqid() . '-';
        }
        $einladungsToken = substr($einladungsToken, 0, -1);

        $stmt = $this->db->get_dbCon()->prepare("INSERT INTO t_einladungscode (einladungscode_code, t_wg_wg_id, einladungscode_name) VALUES(:einladungscode_code, :t_wg_wg_id, :einladungscode_name)");
        $stmt->bindParam(':einladungscode_code', $einladungsToken);
        $stmt->bindParam(':t_wg_wg_id', $t_wg_wg_id);
        $stmt->bindParam(':einladungscode_name', $einladungscode_name);
        $stmt->execute();
        return $einladungsToken;
    }

    private function readEinladungFromDB($einladungscode_code)
    {
        $stmt = $this->db->get_dbCon()->prepare("SELECT * FROM t_einladungscode WHERE einladungscode_code = :einladungscode_code");
        $stmt->bindParam(':einladungscode_code', $einladungscode_code);
        $stmt->execute();
        $result = $stmt->fetchAll();
        foreach ($result as &$row) {
            $this->einladungscode_code = $row['einladungscode_code'];
            $this->t_wg_wg_id = $row['t_wg_wg_id'];
            $this->einladungscode_name = $row['einladungscode_name'];
        }
    }

    public static function deleteEinladungsCodeFromDB($db, $einladungscode_code)
    {
        $stmt = $db->get_dbCon()->prepare("DELETE FROM t_einladungscode WHERE einladungscode_code = :einladungscode_code");
        $stmt->bindParam(':einladungscode_code', $einladungscode_code);
        $stmt->execute();
        return 'quak';
    }

    public static function checkEinladungsCode($db, $einladungscode_code)
    {
        $stmt = $db->get_dbCon()->prepare("SELECT * FROM t_einladungscode WHERE einladungscode_code = :einladungscode_code");
        $stmt->bindParam(':einladungscode_code', $einladungscode_code);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if (count($result) == 1) {
            $output = true;
        } else {
            $output = false;
        }
        return ($output);
    }

    public function deleteEinladungsCode(){
        $stmt = $this->db->get_dbCon()->prepare("DELETE FROM t_einladungscode WHERE einladungscode_code = :einladungscode_code");
        $stmt->bindParam(':einladungscode_code', $this->einladungscode_code);
        $stmt->execute();
    }
}