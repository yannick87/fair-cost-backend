<?php

class SessionToken
{
    private static function deleteUserToken($db, $sessiontoken_user_id){
        $stmt = $db->get_dbCon()->prepare("DELETE FROM t_sessiontoken WHERE sessiontoken_user_id = :sessiontoken_user_id");
        $stmt->bindParam(':sessiontoken_user_id', $sessiontoken_user_id);
        $stmt->execute();
    }

    public static function newToken($db, $sessiontoken_user_id)
    {
        SessionToken::deleteUserToken($db,$sessiontoken_user_id);
        $sessiontoken = "";
        for ($i = 1; $i <= 4; $i++) {
            $sessiontoken .= uniqid() . '-';
        }
        $sessiontoken = substr($sessiontoken, 0, -1);
        $stmt = $db->get_dbCon()->prepare("INSERT INTO t_sessiontoken (sessiontoken, sessiontoken_expired, sessiontoken_user_id) VALUES(:sessiontoken,ADDTIME(now(), '02:00:00'),:sessiontoken_user_id)");
        $stmt->bindParam(':sessiontoken', $sessiontoken);
        $stmt->bindParam(':sessiontoken_user_id', $sessiontoken_user_id);
        $stmt->execute();
        return ($sessiontoken);
    }

    public static function checkToken($db, $sessiontoken)
    {
        $stmt = $db->get_dbCon()->prepare("SELECT * FROM t_sessiontoken WHERE sessiontoken = :sessiontoken");
        $stmt->bindParam(':sessiontoken', $sessiontoken);
        $stmt->execute();
        $result = $stmt->fetchAll();
        if (count($result) == 1) {
            $expire = '';
            $phpTime = date("Y-m-d H:i:s");
            foreach($result as &$row){
                $expire = new DateTime($row['sessiontoken_expired']);
            }
            $expire = $expire->format("Y-m-d H:i:s");

            if($expire < $phpTime){
                $output = false;
            } else {
                $stmt = $db->get_dbCon()->prepare("UPDATE t_sessiontoken SET sessiontoken_expired = ADDTIME(now(), '02:00:00') WHERE sessiontoken = :sessiontoken");
                $stmt->bindParam(':sessiontoken', $sessiontoken);
                $stmt->execute();
                $output = true;
            }

        } else {
            $output = false;
        }
        return ($output);
    }

    public static function checkCreds($db, $user_emailadresse, $user_passwort){
        $stmt = $db->get_dbCon()->prepare("SELECT COUNT(*) as anzahl, user_id FROM t_user WHERE user_emailadresse = :user_emailadresse AND user_passwort = :user_passwort");
        $stmt->bindParam(':user_emailadresse', $user_emailadresse);
        $stmt->bindParam(':user_passwort', $user_passwort);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $countRow = 0;
        $userid = 0;
        foreach ($result as &$row) {
            $countRow = $row['anzahl'];
            $userid = $row['user_id'];
        }
        if($countRow == 1 && $userid > 0){
            return true;
        } else {
            return false;
        }
    }

    public static function checkEmailExists($db, $user_emailadresse){
        $stmt = $db->get_dbCon()->prepare("SELECT COUNT(*) as anzahl, user_id FROM t_user WHERE user_emailadresse = :user_emailadresse");
        $stmt->bindParam(':user_emailadresse', $user_emailadresse);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $countRow = 0;
        $userid = 0;
        foreach ($result as &$row) {
            $countRow = $row['anzahl'];
            $userid = $row['user_id'];
        }
        if($countRow == 1 && $userid > 0){
            return true;
        } else {
            return false;
        }
    }

    public static function getUserID($db, $user_emailadresse, $user_passwort){
        $stmt = $db->get_dbCon()->prepare("SELECT COUNT(*) as anzahl, user_id FROM t_user WHERE user_emailadresse = :user_emailadresse AND user_passwort = :user_passwort");
        $stmt->bindParam(':user_emailadresse', $user_emailadresse);
        $stmt->bindParam(':user_passwort', $user_passwort);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $countRow = 0;
        $userid = 0;
        foreach ($result as &$row) {
            $countRow = $row['anzahl'];
            $userid = $row['user_id'];
        }
        if($countRow == 1 && $userid > 0){
            return $userid;
        } else {
            return 0;
        }
    }

    public static function getUserIDByEmail($db, $user_emailadresse){
        $stmt = $db->get_dbCon()->prepare("SELECT COUNT(*) as anzahl, user_id FROM t_user WHERE user_emailadresse = :user_emailadresse");
        $stmt->bindParam(':user_emailadresse', $user_emailadresse);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $countRow = 0;
        $userid = 0;
        foreach ($result as &$row) {
            $countRow = $row['anzahl'];
            $userid = $row['user_id'];
        }
        if($countRow == 1 && $userid > 0){
            return $userid;
        } else {
            return 0;
        }
    }

    public static function getUserIDByToken($db, $sessionToken){
        $stmt = $db->get_dbCon()->prepare("SELECT sessiontoken_user_id FROM t_sessiontoken WHERE sessiontoken = :sessiontoken");
        $stmt->bindParam(':sessiontoken', $sessionToken);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $userid = 0;
        foreach ($result as &$row) {
            $userid = $row['sessiontoken_user_id'];
        }

        return $userid;

    }

    public static function checkResetTokenExists($db, $resetToken){
        $stmt = $db->get_dbCon()->prepare("SELECT COUNT(*) as anzahl FROM t_passwortReset WHERE passwortReset_Token = :passwortReset_Token");
        $stmt->bindParam(':passwortReset_Token', $resetToken);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $countRow = 0;
        foreach ($result as &$row) {
            $countRow = $row['anzahl'];
        }
        if($countRow == 1){
            return true;
        } else {
            return false;
        }
    }

    public static function unsetToken($db, $userId){
        $stmt = $db->get_dbCon()->prepare("DELETE FROM t_sessiontoken WHERE sessiontoken_user_id = :sessiontoken_user_id");
        $stmt->bindParam(':sessiontoken_user_id', $userId);
        $stmt->execute();
    }

}