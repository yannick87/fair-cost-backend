<?php
class Database
{
    const HOST = 'localhost';
    const DB_NAME = 'fairCostDB';
    const DB_USER = 'fairCostUser';
    const DB_PASSWORD = '7777Nase$';

    private $dbCon;

    public function __construct()
    {
        $host = $this::HOST;
        $db = $this::DB_NAME;
        $user = $this::DB_USER;
        $pw = $this::DB_PASSWORD;

        try {
            $this->dbCon = new PDO("mysql:host=" . $host . ";dbname=" . $db, $user, $pw);
            $this->dbCon->exec("set names utf8");
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }
    }

    public function get_dbCon(){
        return $this->dbCon;
    }
}