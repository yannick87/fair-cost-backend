#Fair Cost Backend

##Introduction
Backend for the fair-cost-app in ESA. In this README we describe the endpoints of the API.

<hr>

##Endpoints

###addBetrag.php
__Description__

Add a new betrag to the user

__TYPE__

POST

__Params:__
* String: sessionToken
* Float: betrag
* String: comment
* File: image

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
}

<hr>

###betragByUserOfMonth.php
__Description__

Returns all betraege of given user by given month

__TYPE__

POST

__Params:__
* String: sessionToken
* INT: userId
* INT: month
* INT: year

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
    "data": [{
            "betragId": INT,
            "betrag": FLOAT,
            "datum": STRING
    }]
}

<hr>

###betragDetail.php
__Description__

Returns details of a single betrag

__TYPE__

POST

__Params:__
* String: sessionToken
* String: betragId

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
    "data": {
        "betrag": FLOAT,
        "comment": STRING,
        "datum": STRING,        
        "image": FILE
    }
}

<hr>

###bilanzByMonth.php
__Description__

Returns sum of betrage for given month, year and wg for each user

__TYPE__

POST

__Params:__
* String: sessionToken
* INT: month
* INT: year

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
    "data": [{
            "userName": STRING,
            "userId": INT,
            "betragSummeMonat": FLOAT
    }]
}

<hr>

###bilanzJob.php
__Description__

...

__TYPE__

GET

__Params:__
NONE

__Response__

NONE

<hr>

###changeWGImage.php
__Description__



__TYPE__

POST

__Params:__
* String: sessionToken
* FILE: image

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
}

<hr>

###changeWGName.php
__Description__

Change name of the given WG

__TYPE__

POST

__Params:__
* String: sessionToken
* String: wgName

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
}

<hr>

###confirmInvitation.php
__Description__

Add user to the given WG and return a sessionToken for the new user

__TYPE__

POST

__Params:__
* String: userMail
* String: userName
* String: userPassword
* String: wgCode

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
    "data": {
        "sessionToken": STRING",
    }
}

<hr>

###confirmTransaction.php
__Description__

...

__TYPE__

POST

__Params:__
* INT: schuldenID
* String: sessionToken

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
}

<hr>

###deleteBetrag.php
__Description__

delte given betrag from db

__TYPE__

POST

__Params:__
* String: sessionToken
* INT: betragId

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
}

<hr>

###deleteOpenInvitations.php
__Description__

delete a given invitation

__TYPE__

POST

__Params:__
* String: sessionToken
* String: einladungsCode

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
}

<hr>

###deleteUserById.php
__Description__

Delete (softdelete) given user from wg. If it is the last active user in this wg delete the wg. If it is an adminuser, choose a new adminuser (lowest userid).

__TYPE__

POST

__Params:__
* String: sessionToken
* String: userId

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
}

<hr>

###deleteWG.php
__Description__

Delete given WG

__TYPE__

POST

__Params:__
* String: sessionToken

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
}

<hr>

###getHomeScreen.php
__Description__

Returns wgName, isAdmin and wgImage for startScreen (wg home)

__TYPE__

POST

__Params:__
* String: sessionToken

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
    "data": {
        "image": FILE,
        "wgName": STRING,
        "isAdmin" BOOLEAN
    }
}

<hr>

###login.php
__Description__

Returns a new sessionToken for given user

__TYPE__

POST

__Params:__
* String: userMail
* String: userPassword

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
    "data": {
        "sessionToken": STRING",
        "userName": STRING",
        "userId": INT"
    }
}

<hr>

###newsFeed.php
__Description__

...

__TYPE__

POST

__Params:__
* String: sessionToken

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
    "data": [{
                 "datum": STRING,
                 "news": STRING
         }]
}

<hr>

###openInvitations.php
__Description__

Returns all open Invitations of given WG

__TYPE__

POST

__Params:__
* String: sessionToken

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
    "data": [{
             "provName": STRING,
             "einladungsCode": STRING
     }]
}

<hr>

###openTransactions.php
__Description__

Returns all open transactions (glaeubiger and schuldner)

__TYPE__

POST

__Params:__
* String: sessionToken

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
    "data": [{
             "glaeubiger": STRING,
             "schuldner": STRING,
             "betrag": FLOAT,
             "schuldenId": INT
     }]
}

<hr>

###registerWG.php
__Description__

Register a new wg and add a user to it, returns a new session token

__TYPE__

POST

__Params:__
* String: userMail
* String: userPassword
* String: userName
* String: wgName
* String: wgImage (optional)~~~~

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
    "data": {
        "sessionToken": STRING"
    }
}

<hr>

###renewSessionToken.php
__Description__

Returns a new sessionToken for given User

__TYPE__

POST

__Params:__
* String: userMail
* String: userPassword

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
    "data": {
        "sessionToken": STRING"
    }
}

<hr>

###resetPW.php
__Description__

Send a password reset link to given email

__TYPE__

POST

__Params:__
* String: userMail

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
}

<hr>

###sendInvitation.php
__Description__

Send a mail to new user

__TYPE__

POST

__Params:__
* String: sessionToken
* String: provMail
* String: provName

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
}

<hr>

###userByWG.php
__Description__

Returns all members of given wg

__TYPE__

POST

__Params:__
* String: sessionToken

__Response__

{
    "error": BOOLEAN,
    "message": STRING,
    "data": [{
             "userName": STRING,
             "userId": INT
     }]
}

<hr>


